bandoctors is a custom emoji showing a snake wrapped around a pin, crossed out.

Licence
-------

bandoctors is available in the public domain for free use and modifcation
without credit. See
[the Creative Commons Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
for more details.

Credit
------

bandoctors is part of the
[puzzlemoji](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
emoji set by [Mary Gardiner](https://mary.gardiner.id.au/).

bandoctors is based on
[Caduceus](https://openclipart.org/detail/177588/caduceus) by
global quiz on openclipart.
