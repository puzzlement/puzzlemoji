nofucks is a custom emoji showing a silhouetted person raising their arms, with
the words "0 fucks" superimposed.

Licence
-------

nofucks is available in the public domain for free use and modifcation
without credit. See
[the Creative Commons Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
for more details.

Credit
------

nofucks is part of the
[puzzlemoji](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
emoji set by [Mary Gardiner](https://mary.gardiner.id.au/).

nofucks is based on [cheer](https://openclipart.org/detail/77143/cheer) by
shokunin on openclipart and inspired by the [look at all the fucks I
give](http://knowyourmeme.com/memes/look-at-all-the-fucks-i-give) images.
