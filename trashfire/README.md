trashfire is a custom emoji showing a garbage can with a fire inside.

Licence
-------

trashfire is available in the public domain for free use and modifcation
without credit. See
[the Creative Commons Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
for more details.

Credit
------

trashfire is part of the
[puzzlemoji](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
emoji set by [Mary Gardiner](https://mary.gardiner.id.au/).

trashfire is based on [Spam Mail to
trash](https://openclipart.org/detail/95077/spam-mail-to-trash) by kg on
openclipart.
