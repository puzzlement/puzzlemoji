banbrains is a custom emoji showing a cross-section of a human head containing
a brain, crossed out in red.

Licence
-------

banbrains is available in the public domain for free use and modifcation
without credit. See
[the Creative Commons Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
for more details.

Credit
------

banbrains is part of the
[puzzlemoji](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
emoji set by [Mary Gardiner](https://mary.gardiner.id.au/).

banbrains is based on [Silhouette of a
brain](https://openclipart.org/detail/23338/silhouette-of-a-brain) by laobc on
openclipart.
