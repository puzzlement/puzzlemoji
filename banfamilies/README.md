banfamilies is a custom emoji showing a stylised couple, crossed out.

Licence
-------

banfamilies is available in the public domain for free use and modifcation
without credit. See
[the Creative Commons Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
for more details.

Credit
------

banfamilies is part of the
[puzzlemoji](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
emoji set by [Mary Gardiner](https://mary.gardiner.id.au/).

banfamilies is based on [Family](https://openclipart.org/detail/147937/family)
by gsagri04 on openclipart.
