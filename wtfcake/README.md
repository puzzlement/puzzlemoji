wtfcake is a custom emoji showing a birthday cake with the letters WTF?
superimposed.

Licence
-------

wtfcake is available in the public domain for free use and modifcation without
credit. See
[the Creative Commons Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
for more details.

Credit
------

wtfcake is part of the
[puzzlemoji](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
emoji set by [Mary Gardiner](https://mary.gardiner.id.au/).

wtfcake is based on [cake](https://openclipart.org/detail/2566/cake) by
Machovka on openclipart and inspired by Ann Larie Valentine's [WTF
cake](https://www.flickr.com/photos/sanfranannie/3178105727/).
