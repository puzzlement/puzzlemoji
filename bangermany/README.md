bangermany is a custom emoji showing a German flag crossed out.

Licence
-------

bangermany is available in the public domain for free use and modifcation
without credit. See
[the Creative Commons Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
for more details.

Credit
------

bangermany is part of the
[puzzlemoji](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
emoji set by [Mary Gardiner](https://mary.gardiner.id.au/).

bangermany is based on [Schwarz Rot
Gold](https://openclipart.org/detail/95263/schwarz-rot-gold) by skotan on
openclipart.
