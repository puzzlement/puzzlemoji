nopetopus is a custom emoji showing a red octopus superimposed with the word
"nope".

Licence
-------

nopetopus is available in the public domain for free use and modifcation
without credit. See
[the Creative Commons Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
for more details.

Credit
------

nopetopus is part of the
[puzzlemoji](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
emoji set by [Mary Gardiner](https://mary.gardiner.id.au/).

nopetopus is based on [Red Cartoon
Octopus](https://openclipart.org/detail/204658/red-cartoon-octopus) by j4p4n on
openclipart and inspired by [the Nopetopus Tumblr
image](http://lauralex.tumblr.com/post/25770120130/the-nopetopus) popularised
by lauralex.
