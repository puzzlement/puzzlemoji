banusa is a custom emoji showing a map of the USA crossed out.

Licence
-------

banusa is available in the public domain for free use and modifcation
without credit. See
[the Creative Commons Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
for more details.

Credit
------

banusa is part of the
[puzzlemoji](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
emoji set by [Mary Gardiner](https://mary.gardiner.id.au/).

banusa is based on
[USA map and flag](https://openclipart.org/detail/212654/usa-map-and-flag) by
cybescooty on openclipart.
