banaustralia is a custom emoji showing a map of Australia crossed out.

Licence
-------

banaustralia is available in the public domain for free use and modifcation
without credit. See
[the Creative Commons Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
for more details.

Credit
------

banaustralia is part of the
[puzzlemoji](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
emoji set by [Mary Gardiner](https://mary.gardiner.id.au/).

banaustralia is based on
[Australia Outline](https://openclipart.org/detail/16210/australia-outline) by
MrTim on openclipart.
