The puzzlemoji are custom emoji by [Mary
Gardiner](https://mary.gardiner.id.au/). [Learn more and see
examples.](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
	
Licence
-------

puzzlemoji are available in the public domain for free use and modifcation
without credit. See [the Creative Commons Public Domain
Dedication](https://creativecommons.org/publicdomain/zero/1.0/) for more
details.

Credit
------

See the README.md in individual emoji folders for source image credits.
