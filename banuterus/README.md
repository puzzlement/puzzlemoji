banuterus is a custom emoji showing a human uterus crossed out with a red line.

Licence
-------

banuterus is available in the public domain for free use and modifcation
without credit. See
[the Creative Commons Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
for more details.

Credit
------

banuterus is part of the
[puzzlemoji](https://puzzling.org/creations/2016/05/puzzlemoji-wtf-cake-nopetopus-ban-australia-and-more)
emoji set by [Mary Gardiner](https://mary.gardiner.id.au/).

banuterus is based on [Wiki emaka
väärarengud](https://commons.wikimedia.org/wiki/File:Wiki_emaka_v%C3%A4%C3%A4rarengud.jpg)
by Women's Health and Education Center on Wikimedia Commons.
